
#define TYPE_GET    1
#define TYPE_PUT    2
#define TYPE_REP    3   //reply
#define TYPE_OK     4   

struct msg_client
{
    short 	type;
    char 	msg[100];
    int 	contentSize;
};

struct msg_server
{
    short 		type;
    char 		date[100];
    char 		server[100];
    char 		ipAddress[100];
    uint16_t	clientport;
    int 		contentSize;
};

