#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>     //malloc

#include <netdb.h>		//getaddrinfo

#include "msg.h"





#define PORT		"7776"


/* prototypes */
void put(void);
void get(void);
void con(void);
void sendCommand (struct msg_client message);
int extractCommand(char* src, char* file, char* address, char* port);


  int                   s_tcp;          // socket descriptor
  struct sockaddr_in    sa;             // socket address structure
  struct addrinfo       hints;          // specify hints for call getaddrinfo
  struct addrinfo*      result;         // result linked list for call getaddrinfo
  int                   sa_len = sizeof(struct sockaddr_in), n;
  char*                 content;        //content received from server


struct msg_client clientMsg;             //struct msg to be sent to server 
struct msg_server serverMsg;    //struct msg_server to be received from server

char buff [200];			//buffer for terminal input

char* 	filename;
char* 	addr;
char	address[50];
char* 	port;
char 	file[50];
int 	filenamelength;
int		addressLength;


FILE* pFile;		//file descriptor
char* content;		//buffer for content of file


int main ( )
{
    printf("!!! WELCOME BUDDY!!!\n");

    // MMI
    while(1){

    	memset(file, '\0', sizeof(file));
        printf("ENTER COMMAND: ");
    
    
        if( fgets( buff, 100, stdin ) != 0 ){
            
            /*** COMMAND quit ***/
            if ( 0==strncmp( buff, "quit\n",5) ){
                printf("Good Bye\n");
                close(s_tcp);
                _exit(1);
                
        	/*** COMMAND get ***/
            } else if ( 0==strncmp( buff, "get ",4) ){
    
                addr 		= strchr( buff, '@');
                filename 	= strchr( buff, ' ' );
                port		= strchr( addr, ' ' );
                
                if( ( addr != 0 ) && ( filename != 0 ) && (port != 0 ) ){; 
                
                	*port++;
        	        strcpy( strstr(port, "\n"),"\0");		//replace \n with \0
                	printf("extracted Port: %s\n", port);
                    *addr++; //addr string 
                    
                    addressLength = (port - addr)-1;
                    strncpy( address, addr, addressLength );
                    printf("address: %s \nlength:%d\n",address, addressLength);
                    
                    *filename++;
                    filenamelength = (addr - filename)-1;
                    strncpy( file, filename, filenamelength );
                    printf("filename: %s \nlength:%d\n",file, filenamelength);
    			

                //if ( extractCommand(buff, file, address, port) == EXIT_SUCCESS ) {
                
                    con();	//set up connection
                    get();	//send get command
                    
                } else { 
                	printf("UNKNOWN COMMAND\n"); 
                }//if
                
            /*** COMMAND put ***/
            } else if ( 0==strncmp( buff, "put ",4) ){
            
                addr 		= strchr( buff, '@');
                filename 	= strchr( buff, ' ' );
                port		= strchr( addr, ' ' );

                
                if( ( addr != 0 ) && ( filename != 0 ) && ( port != 0) ){
                	
                	*port++;
        	        strcpy( strstr(port, "\n"),"\0");		//replace \n with \0
                	printf("extracted Port: %s\n", port);
                    *addr++; //addr string 
                    
                    addressLength = (port - addr)-1;
                    strncpy( address, addr, addressLength );
                    printf("address: %s \nlength:%d\n",address, addressLength);
                    
                    *filename++;
                    filenamelength = (addr - filename)-1;
                    strncpy( file, filename, filenamelength );
                    printf("filename: %s \nlength:%d\n",file, filenamelength);
                    
                    
                //if ( extractCommand(buff, file, address, port) == EXIT_SUCCESS ) {

            		con();
            		put();
            	
            	} else {
            		printf("UNKNOWN COMMAND\n"); 
            	}//if
        	
        	/*** COMMAND unknown ***/
            } else {
                printf("UNKNOWN COMMAND\n");
            }
              
        }  
    }//while
    
    close(s_tcp);
}//main


void sendCommand(struct msg_client message) {
        if((n=send(s_tcp, &message, sizeof(struct msg_client),0)) > 0) {
                //printf("Message %s sent ( %i Bytes).\n", message.msg, n); 
        }//if
}//sendCommand


int extractCommand(char* src, char* file, char* address, char* port) { 
	
	/* local variables */
	char* 	filename;
	char* 	addr;
	int		filenameLength;
	int		addressLength;
	
	
	addr 		= strchr(  src, '@' );
    filename 	= strchr(  src, ' ' );
    port		= strchr( addr, ' ' );
    
    if( ( addr == 0 ) || ( filename == 0 ) || (port == 0 ) ){
    	return EXIT_FAILURE;
    }//if
    
    /* extract port number */
    *port++;
    strcpy( strstr(port, "\n"),"\0");		//replace \n with \0
    printf("extracted Port: %s\n", port);
    
    /* extract address */
    *addr++;                
    addressLength = (port - addr)-1;
    strncpy( address, addr, addressLength );
    printf("address: %s \nlength:%d\n",address, addressLength);
                    
	/* extract filename */
    *filename++;
    filenameLength = (addr - filename)-1;
    strncpy( file, filename, filenameLength );
    printf("filename: %s \nlength:%d\n",file, filenameLength);
    
    
	return EXIT_SUCCESS;
	
}//extractCommand


void get(){

    memset(&clientMsg, 0, sizeof(struct msg_client));
    clientMsg.type    = TYPE_GET;
    strncpy(clientMsg.msg, file, filenamelength);
    sendCommand(clientMsg);

    
    if ( recv(s_tcp, &serverMsg,sizeof(struct msg_server), 0) ) {
        printf("+++ Reply from server +++\n");
        printf("Date: %s", serverMsg.date);
        printf("Servername: %s\n", serverMsg.server);
        printf("IP-Address: %s\n", serverMsg.ipAddress);
    }//if
    if( !( serverMsg.contentSize == -1) ){
        content = malloc(serverMsg.contentSize);
        if (content != NULL) {
            if ( recv(s_tcp, content, serverMsg.contentSize, 0) ) {
                printf("File content:\n%s\n", content);
            }//if
            free(content);
        }//if
    } else { printf("FILE NOT FOUND\n"); }//if
}//get




void put() {
	
	/* setup message to be sent to server */
	memset(&clientMsg, 0, sizeof(struct msg_client));
    clientMsg.type    = TYPE_PUT;
    strncpy(clientMsg.msg, file, filenamelength);
    
    printf("filename= %s\n", file);
    
    /* open file */
    pFile = fopen(file, "rb");
    if (pFile == NULL){
    	perror("open:");
    	printf("Couldn't open file. Wrong file path?\n");
    	
	}else {
				
		// get size of file
		fseek(pFile,0,SEEK_END);
		clientMsg.contentSize = ftell(pFile);
		fseek(pFile,0,SEEK_SET);
    
    	sendCommand(clientMsg);
    
    	content = malloc(clientMsg.contentSize);
		
		if (fread(content, 1, clientMsg.contentSize, pFile) < 0) {
			printf("read failed\n");
		}//if

		/* send content to client */
		if((n=send(s_tcp, content, clientMsg.contentSize, 0)) > 0) {
			printf("File sent ( %i Bytes).\n", n); 
		} else {
			perror("Send:");
		}//if

		free(content);
		fclose(pFile);
		//printf("close\n");
    
		/* wait for reply from server */
		if ( recv(s_tcp, &serverMsg, sizeof(struct msg_server), 0) ) {
			printf("+++ Reply from server +++\n", serverMsg.date);
			printf("Date: %s", serverMsg.date);
		}//if
	
		if ( !(serverMsg.type == TYPE_OK) ) {
			printf("Received unexpected reply from server\n");
		} else {
			printf("Message: OK <%s>:<%d>\n", serverMsg.server, serverMsg.clientport);
		}//if
		
    }//if
}//put



void con(void) {

 	/* setup hints for getaddrinfo */
   memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_family     = AF_UNSPEC;    /* Allow IPv4 or IPv6 */
    hints.ai_socktype   = SOCK_STREAM;  /* Datagram socket */
    hints.ai_flags      = AI_PASSIVE;   /* For wildcard IP address */
    hints.ai_protocol   = IPPROTO_TCP;  /* Any protocol, 0 for any protocol */
    hints.ai_canonname  = NULL;
    hints.ai_addr       = NULL;
    hints.ai_next       = NULL;

	/* get linked list of structs addrinfo */
	if ( getaddrinfo(address, port, &hints, &result) < 0 ) {
		perror("GetAddressInfo");
		_exit(1);
		
	} else {
		//printf("Yay, got list of addrinfo\n");
		/* iterate over list of addrinfo */
		int counter = 1;
		do {
			printf("%d: Family= %d, SocketType= %d, Protocol= %d\n", 
				counter++, 
				result->ai_family,
				result->ai_socktype,
				result->ai_protocol);
			

			/* create a socket */
			if ( (s_tcp=socket(result->ai_family, result->ai_socktype, result->ai_protocol)) <0 ) {
				perror("TCP Socket");
				_exit(1);
			} else {
				//printf("Created socket\n");
			}//if


			/* try to connect via socket */
			if ( connect(s_tcp, result->ai_addr, result->ai_addrlen ) <0 ) {
				perror("Connect");

			} else {
				//printf("Succesfully connected\n");
				break;
			}//if
				
		} while ((result = result->ai_next) != NULL);
	
	}//else
        
}//con



