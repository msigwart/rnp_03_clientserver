#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#include <netdb.h>		//getaddrinfo
#include <time.h>
#include <fcntl.h>
#include <string.h>

#include "msg.h"


#define PORT		"7776"


/* prototypes */
int handlePutCmd();
int handleGetCmd();


  int 						s_tcp, news;		//socket descriptor
  struct sockaddr_storage 	addrInfo;			//socket address structure
  struct addrinfo			hints;
  struct addrinfo*			result;
  unsigned int 				addrInfoLength = sizeof(struct sockaddr_storage), n;
  char* 			    	content;        //for content of file
  struct msg_client     	clientMsg;      //message to be received from client
  struct msg_server     	serverMsg;      //message to be sent to client

  time_t                	rawtime;
  struct tm*            	timeinfo;       //struct for current time

  FILE*                 	pFile;          //file descriptor



int main ( )
{

	/* setup hints struct */
	memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE; // use my IP


	/* get linked list of structs addrinfo */
	if ( getaddrinfo("::", PORT, &hints, &result) < 0 ) {
		perror("GetAddressInfo");
		_exit(1);
		
	} else {
		printf("Yay, got list of addrinfo\n");
		/* iterate over list of addrinfo */
		int counter = 1;
		do {
			printf("%d: Family= %d, SocketType= %d, Protocol= %d\n", 
				counter++, 
				result->ai_family,
				result->ai_socktype,
				result->ai_protocol);
				
			if (result->ai_family == AF_INET6) {
				printf("IPv6\n");
			}//if
			
			/* create socket */
			if ((s_tcp=socket(result->ai_family, result->ai_socktype, result->ai_protocol)) < 0) {
				perror("TCP Socket");
				_exit(1);	//for mac, maybe just exit(1) for linux
				
			}//if
			printf("created socket \n");
			
			/* bind socket */
			if (bind(s_tcp, result->ai_addr, result->ai_addrlen) < 0) {
				perror("bind");
				_exit(1);
			}//if
			printf("bind successful\n");
				
			break;
							
		} while ((result = result->ai_next) != NULL);

	}//else


  	if (listen(s_tcp,5) < 0) {
  		perror("listen");
		close(s_tcp);
		_exit(1);
  	}//if
  	

	while( 1 ) {
	  	printf("Waiting for new tcp connection ... \n");
	  	
	  	/* accept new client */
		if ((news=accept(s_tcp, NULL, NULL)) < 0) {
			perror("accept");
			close(s_tcp);
			_exit(1);
		}//if


		if (recv(news, &clientMsg, sizeof(struct msg_client), 0)) {
			printf("Message received:\n");

			switch (clientMsg.type) {

		        case TYPE_GET:
		        	handleGetCmd();
		        	close(news);
			        break;

		        case TYPE_PUT:
		        	handlePutCmd();
		        	close(news);
			        break;
			        
		        default:
			        break;

			}//switch
		}//if
	}//while

  close(s_tcp);
}//main



int handleGetCmd() {


	printf("GET command: file: %s\n", clientMsg.msg);
	serverMsg.type = TYPE_REP;
	
	//get time and date
	time(&rawtime);
	timeinfo = localtime (&rawtime);
	strncpy(serverMsg.date, asctime (timeinfo ), 100);
	
	//server hostname
	gethostname(serverMsg.server, sizeof(serverMsg.server)); 
	
	
	//ip address of server
	getsockname(news, (struct sockaddr *) &addrInfo, &addrInfoLength);
	if (addrInfo.ss_family == AF_INET) {
		struct sockaddr_in *s = (struct sockaddr_in*) &addrInfo;
		inet_ntop(AF_INET, &s->sin_addr, serverMsg.ipAddress, sizeof(serverMsg.ipAddress));
	}//if
	if (addrInfo.ss_family == AF_INET6) {
		struct sockaddr_in6 *s = (struct sockaddr_in6*) &addrInfo;
		inet_ntop(AF_INET6, &s->sin6_addr, serverMsg.ipAddress, sizeof(serverMsg.ipAddress));
	}//if
	
	
	//open file
	pFile = fopen(clientMsg.msg, "r");
	if (pFile == NULL){
		serverMsg.contentSize = -1;
		
	} else {
		printf("Message received:\n");
		// get size of file
		fseek(pFile,0,SEEK_END);
		serverMsg.contentSize = ftell(pFile);
		fseek(pFile,0,SEEK_SET);
		
	}//if
	
	/* send reply to client */
	if((n=send(news, &serverMsg, sizeof(struct msg_server),0)) > 0) {
		printf("Reply sent ( %i Bytes).\n", n); 
	}//if
	
	if (pFile != NULL) {
		content = malloc(serverMsg.contentSize);
		
		if (fread(content, 1, serverMsg.contentSize, pFile) < 0) {
			printf("read failed\n");
		}//if

		/* send content to client */
		if((n=send(news, content, serverMsg.contentSize, 0)) > 0) {
			printf("Reply sent ( %i Bytes).\n", n); 
		} else {
			perror("Send:");
		}//if

		/* free memory of content and close file */
		free(content);
		fclose(pFile);
		printf("close\n");
	}//if

    return EXIT_SUCCESS;
                    
}//handleGetCmd



int handlePutCmd() {
	int number;		//number of written bytes

	printf("PUT command: file: %s, filesize %d\n", clientMsg.msg, clientMsg.contentSize);
	serverMsg.type = TYPE_OK;
	
	/* allocate memory */
	content = (char*) malloc(clientMsg.contentSize*sizeof(char));
	if (content == NULL) {
		printf("malloc failed\n");
		return EXIT_FAILURE;
	}//if
	
	/* receive file content */
	recv(news, content, clientMsg.contentSize, 0);
	printf("File content received: %s\n", content);
	
	/* open file */
	pFile = fopen(clientMsg.msg, "wb");
	if (pFile == NULL) {
		printf("Couldn't open file\n");
		return EXIT_FAILURE;	
	}//if
	printf("opened file\n");
	
	/* write into file */	//TODO sizeof char
	number = fwrite(content, sizeof(char), clientMsg.contentSize, pFile);
	if (number < clientMsg.contentSize ) {
		printf("Writing to file failed\n");
		return EXIT_FAILURE;
	}//if
	printf("wrote file\n");
	
	/* free memory of content and close file */
	free(content);
	fclose(pFile);
	printf("closed file\n");
	
	//get time and date
	time(&rawtime);
	timeinfo = localtime (&rawtime);
	strncpy(serverMsg.date, asctime (timeinfo ), 100);
	
	//get clienthostname & clientport
	getpeername(news, (struct sockaddr*)&addrInfo, &addrInfoLength);
	if (addrInfo.ss_family == AF_INET) {
		struct sockaddr_in *s = (struct sockaddr_in *)&addrInfo;
		inet_ntop(AF_INET, &s->sin_addr, serverMsg.server, sizeof(serverMsg.server));
		serverMsg.clientport = ntohs(s->sin_port);
	}//if
	if (addrInfo.ss_family == AF_INET6) {
		struct sockaddr_in6 *s = (struct sockaddr_in6 *)&addrInfo;
		inet_ntop(AF_INET6, &s->sin6_addr, serverMsg.server, sizeof(serverMsg.server));
		serverMsg.clientport = ntohs(s->sin6_port);
	}//if
	


	/* send reply to client */
	if((n=send(news, &serverMsg, sizeof(struct msg_server),0)) <= 0) {
		printf("send failed\n");
		return EXIT_FAILURE;
	}//if
	
	printf("OK reply sent ( %i Bytes).\n", n); 
		
	
	return EXIT_SUCCESS;

	
}//receivedPutCmd


